<?php
use App\Http\Controllers\Controlador;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/inicio', function () {
    
    return view('dashboard_inicio');
})->name('inicio');

Route::get('/inicio2', function () {
    
    return view('dashboard_inicio');
})->name('inicio');

Route::get('/CrearCuenta', function () {
    return view('CrearCuenta');
})->name('CrearCuenta');

Route::post('/CrearCuenta', [Controlador::class, "crearCuenta"])->name("enviarCrearCuenta");

Route::get('/TipoTransaccion', function () {
    return view('TipoTransaccion');
})->name('TipoTransaccion');

Route::get('/Retiro', function () {
    return view('Retiro');
})->name('Retiro');

Route::post('/Retiro', [Controlador::class, "realizarRetiro"])->name("realizarRetiro");


Route::get('/Deposito', function () {
    return view('Deposito');
})->name('Deposito');

Route::post('/Deposito', [Controlador::class, "realizarDeposito"])->name("realizarDeposito");



Route::get('/ConsultaSaldo', function () {
    return view('ConsultaSaldo');
})->name('ConsultaSaldo');


Route::post('/consultarSaldo', [Controlador::class, "consultarSaldo"])->name("consultarSaldo");


Route::get('/TodosClientes',[Controlador::class, "index"])->name("TodosClientes");




Route::get('/ReporteMes',[Controlador::class, "ReporteMes"])->name("ReporteMes");

Route::get('/ConsultaC',[Controlador::class, "obtenerConsultaC"])->name("ConsultaC");


Route::get('/ConsultaC',[Controlador::class, "obtenerConsultaC"])->name("ConsultaC");


Route::get('/CrearCliente', function () {
    return view('RegistrarCliente');
})->name('CrearCliente');


Route::post('/CrearCliente', [Controlador::class, "crearUsuario"])->name("enviarCrearUsuario");

Route::get('/ActivarCuenta', function () {
    return view('ActivarCuenta');
})->name('ActivarCuenta');

Route::post('/ActivarCuenta', [Controlador::class, "activarCuenta"])->name("enviarActivarCuenta");


Route::post('/',[Controlador::class, "login"])->name("login");


Route::get('/Monitoreo',[Controlador::class, "monitoreo"])->name("Monitoreo");
