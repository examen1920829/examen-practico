<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Controlador extends Controller
{
    public function index()
    {
        $query = "
            SELECT
                usu.numero_identificacion,
                usu.tipo_documento,
                usu.nombres,
                usu.apellidos,
                usu.razon_social,
                usu.municipio,
                usu.departamento,
                COUNT(CASE WHEN c.activo = true THEN c.numero_cuenta END) AS cuentas_activas,
                COUNT(CASE WHEN c.activo = false THEN c.numero_cuenta END) AS cuentas_inactivas
            FROM
                usuarios usu
            LEFT JOIN
                cuentas c ON usu.numero_identificacion = c.usuario_numero_identificacion
            GROUP BY
                usu.numero_identificacion, usu.tipo_documento, usu.nombres, usu.apellidos, usu.razon_social, usu.municipio, usu.departamento
        ";
    
        $datos = DB::select($query);
    
        return view("TodosClientes", compact('datos'));
    }


    public function login(Request $request)
    {
        $numeroIdentificacion = $request->input('numeroIdentificacion');
        $password = $request->input('password');


        $query = "SELECT * FROM usuarios WHERE numero_identificacion = ?";

        $usuario = DB::selectOne($query, [$numeroIdentificacion]);

        if ($usuario && password_verify($password, $usuario->contraseña)) {
            return redirect()->route('inicio');
        } else {
            return redirect()->back()->with('error', 'Credenciales incorrectas.');
        }
    }

    public function activarCuenta(Request $request)
    {
        $numeroCuenta = $request->input('numeroCuenta');

        $query = "UPDATE cuentas SET activo = true WHERE numero_cuenta = ?";

        DB::update($query, [$numeroCuenta]);

        return response()->json(['success' => true]);
    }

    public function crearUsuario(Request $request)
    {
        $tipoDocumento = $request->input('tipo_documento');
        $numeroIdentificacion = $request->input('numero_identificacion');
        $nombres = $request->input('nombres');
        $apellidos = $request->input('apellidos');
        $razonSocial = $request->input('razon_social');
        $municipio = $request->input('municipio');
        $departamento = $request->input('departamento');
        $contraseña = $this->encriptarContraseña($request->input('contraseña'));

        $query = "INSERT INTO usuarios (tipo_documento, numero_identificacion, nombres, apellidos, razon_social, municipio, departamento, contraseña)
                  VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        DB::insert($query, [$tipoDocumento, $numeroIdentificacion, $nombres, $apellidos, $razonSocial, $municipio, $departamento, $contraseña]);

        return response()->json(['success' => true]);
    }

    private function encriptarContraseña($password)
    {
        $hash = password_hash($password, PASSWORD_DEFAULT);
        return $hash;
    }

    public function obtenerConsultaC()
    {
        $query = "
            SELECT
                usu.numero_identificacion,
                usu.tipo_documento,
                usu.nombres,
                usu.apellidos,
                usu.razon_social,
                usu.municipio,
                usu.departamento
            FROM
                usuarios usu
            INNER JOIN
                cuentas c ON usu.numero_identificacion = c.usuario_numero_identificacion
            INNER JOIN
                transacciones t ON c.numero_cuenta = t.cuenta_numero_cuenta
            WHERE
                c.saldo > 20000 AND
                t.tipo_transaccion = 'Deposito' AND
                t.fecha >= NOW() - INTERVAL '15 days'
            GROUP BY
                usu.numero_identificacion, usu.tipo_documento, usu.nombres, usu.apellidos, usu.razon_social, usu.municipio, usu.departamento
        ";
    
        $datos = DB::select($query);
    
        return view("ConsultaC", compact('datos'));
    }
    

    public function ReporteMes()
    {
        $query = "
            SELECT
                DATE_TRUNC('month', t.fecha) AS mes,
                SUM(c.saldo) AS saldo_total
            FROM
                transacciones t
            INNER JOIN
                cuentas c ON t.cuenta_numero_cuenta = c.numero_cuenta
            GROUP BY
                mes
            ORDER BY
                mes
        ";
    
        $datos = DB::select($query);
    
        return view("ReporteSaldoTotalMes", compact('datos'));
    }
        


    public function realizarDeposito(Request $request)
    {
        $cuentaOrigen = $request->input('cuentaOrigen');
        $cuentaDestino = $request->input('cuentaDestino');
        $valorDeposito = $request->input('valorDeposito');
        $saldoOrigenQuery = "SELECT saldo FROM Cuentas WHERE numero_cuenta = ?";
        $saldoOrigen = DB::selectOne($saldoOrigenQuery, [$cuentaOrigen])->saldo;
    
        if ($saldoOrigen >= $valorDeposito) {
            $nuevoSaldoOrigen = $saldoOrigen - $valorDeposito;
            $actualizarSaldoOrigenQuery = "UPDATE Cuentas SET saldo = ? WHERE numero_cuenta = ?";
            DB::update($actualizarSaldoOrigenQuery, [$nuevoSaldoOrigen, $cuentaOrigen]);
            $saldoDestinoQuery = "SELECT saldo FROM Cuentas WHERE numero_cuenta = ?";
            $saldoDestino = DB::selectOne($saldoDestinoQuery, [$cuentaDestino])->saldo;
            $nuevoSaldoDestino = $saldoDestino + $valorDeposito;
            $actualizarSaldoDestinoQuery = "UPDATE Cuentas SET saldo = ? WHERE numero_cuenta = ?";
            DB::update($actualizarSaldoDestinoQuery, [$nuevoSaldoDestino, $cuentaDestino]);
            $tipoTransaccion = "Deposito";
            $insertarTransaccionQuery = "INSERT INTO Transacciones (tipo_transaccion, fecha, cuenta_numero_cuenta) VALUES (?, CURRENT_DATE, ?)";
            DB::insert($insertarTransaccionQuery, [$tipoTransaccion, $cuentaDestino]);
    
            return response()->json(['success' => true]);
        } else {
            return response()->json(['error' => 'La cuenta de origen no tiene suficiente saldo para realizar el depósito.']);
        }
    }
    


    public function realizarRetiro(Request $request)
    {
        $numeroCuenta = $request->input('numeroCuenta');
        $valorRetiro = $request->input('valorRetiro');
        $saldoQuery = "SELECT saldo FROM Cuentas WHERE numero_cuenta = ?";
        $saldo = DB::selectOne($saldoQuery, [$numeroCuenta])->saldo;
        if ($saldo >= $valorRetiro) {
            $nuevoSaldo = $saldo - $valorRetiro;
            $queryNuevoSaldo = "UPDATE Cuentas SET saldo = ? WHERE numero_cuenta = ?";
            DB::update($queryNuevoSaldo, [$nuevoSaldo, $numeroCuenta]);
            $tipoTransaccion = "Retiro";
            $transaccionInsert = "INSERT INTO Transacciones (tipo_transaccion, fecha, cuenta_numero_cuenta) VALUES (?, CURRENT_DATE, ?)";
            DB::insert($transaccionInsert, [$tipoTransaccion, $numeroCuenta]);
            return response()->json(['success' => true]);
        } else {
            return response()->json(['error' => 'El valor de retiro es mayor al saldo disponible.']);
        }
    }

    public function consultarSaldo(Request $request)
    {
        $numeroCuenta = $request->input('numeroCuenta');
        $query_listado_transacciones = "SELECT tipo_transaccion, fecha
        FROM Transacciones
        WHERE cuenta_numero_cuenta = ?
        ORDER BY fecha DESC
        LIMIT 10;
        ";
        $listado_t = DB::select($query_listado_transacciones, [$numeroCuenta]);
    
        $query = "SELECT saldo FROM cuentas WHERE numero_cuenta = ?";
        $saldo = DB::select($query, [$numeroCuenta]);
    
        if (!empty($saldo)) {
            $saldo = $saldo[0]->saldo; 
            return response()->json([
                'saldo' => $saldo, 
                'transacciones' => $listado_t
            ]);
        } else {
            return response()->json(['saldo' => 'Error: El número de cuenta ingresado no existe.']);
        }
    }

    public function crearCuenta(Request $request)
    {
        $numeroCuenta = $this->generarNumeroCuenta();
        $tipoCuenta = "Ahorro";
        $saldo = $request->input('monto');
        $activo = false;
        $numeroIdentificacion = $request->input('numeroIdentificacion');
        $query = "INSERT INTO cuentas (numero_cuenta, tipo_cuenta, saldo, activo, usuario_numero_identificacion)
                  VALUES (?, ?, ?, ?, ?)";

        DB::insert($query, [$numeroCuenta, $tipoCuenta, $saldo, $activo, $numeroIdentificacion]);
        return response()->json(['success' => true]);
    }

    private function generarNumeroCuenta()
    {
        $numeroCuenta = "";
        for ($i = 0; $i < 13; $i++) {
            $digito = rand(0, 9);
            $numeroCuenta .= $digito;
        }
        return $numeroCuenta;
    }

    public function monitoreo()
    {
        $query = "SELECT usuarios.nombres, usuarios.apellidos, cuentas.numero_cuenta, transacciones.fecha, 
        transacciones.tipo_transaccion
        FROM transacciones
        JOIN cuentas ON transacciones.cuenta_numero_cuenta = cuentas.numero_cuenta
        JOIN usuarios ON cuentas.usuario_numero_identificacion = usuarios.numero_identificacion
        WHERE transacciones.tipo_transaccion IN ('Deposito', 'Retiro')";

        $resultado = DB::select($query);

        return view('Monitoreo', compact('resultado'));
    }
}
