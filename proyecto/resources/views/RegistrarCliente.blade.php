@extends('dashboard_inicio')

@section('content')
    <h1>Crear Usuario</h1>
    <div class="container mt-4">
        <form method="POST" id="formulario" action="{{ route('enviarCrearUsuario') }}">
            @csrf
            <div class="mb-3">
                <input type="text" name="tipo_documento" placeholder="Ingrese el tipo de documento" class="form-control" required>
            </div>
            <div class="mb-3">
                <input type="text" name="numero_identificacion" placeholder="Ingrese el número de identificación" class="form-control" required>
            </div>
            <div class="mb-3">
                <input type="text" name="nombres" placeholder="Ingrese los nombres" class="form-control" required>
            </div>
            <div class="mb-3">
                <input type="text" name="apellidos" placeholder="Ingrese los apellidos" class="form-control" required>
            </div>
            <div class="mb-3">
                <input type="password" name="contraseña" placeholder="Ingrese una contraseña" class="form-control" required>
            </div>
            <div class="mb-3">
                <input type="text" name="razon_social" placeholder="Ingrese la razón social" class="form-control" required>
            </div>
            <div class="mb-3">
                <input type="text" name="municipio" placeholder="Ingrese el municipio" class="form-control" required>
            </div>
            <div class="mb-3">
                <input type="text" name="departamento" placeholder="Ingrese el departamento" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary">Crear Usuario</button>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#formulario").submit(function(event) {
                event.preventDefault();
                var formData = {
                    tipo_documento: $("input[name='tipo_documento']").val(),
                    numero_identificacion: $("input[name='numero_identificacion']").val(),
                    nombres: $("input[name='nombres']").val(),
                    apellidos: $("input[name='apellidos']").val(),
                    razon_social: $("input[name='razon_social']").val(),
                    municipio: $("input[name='municipio']").val(),
                    departamento: $("input[name='departamento']").val(),
                    contraseña: $("input[name = 'contraseña']").val(),
                    _token: $("input[name='_token']").val()
                };

                $.ajax({
                    url: "{{ route('enviarCrearUsuario') }}",
                    type: "POST",
                    data: formData,
                    success: function(response) {
                        alert("Se creó el usuario con éxito.");
                        window.location.href = "{{ route('inicio') }}";
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.error("Error: " + errorThrown);
                    }
                });
            });
        });
    </script>
@endsection
