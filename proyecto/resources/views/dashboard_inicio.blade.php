<!DOCTYPE html>
<html>
<head>
    <title>Registro de Persona</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <style>
    
        .navbar {
            background-color: #34a2b0; 
        }

        .navbar-brand {
            color: #ffffff; 
            font-size: 24px;
        }

        .nav-link {
            color: #ffffff; 
            font-size: 18px;
            margin-right: 15px;
        }

        .nav-link:hover {
            color: #f7f7f7; 
        }
    </style>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand" onclick="window.location='{{ route('inicio') }}'; return false;">Inicio</a>
            <form method="GET">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" onclick="window.location='{{ route('TipoTransaccion') }}'; return false;">Transacciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="window.location='{{ route('ConsultaSaldo') }}'; return false;">Consulta de saldo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="window.location='{{ route('TodosClientes') }}'; return false;">Consulta Clientes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="window.location='{{ route('CrearCuenta') }}'; return false;">Crear Cuenta</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="window.location='{{ route('ReporteMes') }}'; return false;">Reporte Mes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="window.location='{{ route('ConsultaC') }}'; return false;">Consulta C</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="window.location='{{ route('ActivarCuenta') }}'; return false;">Activar Cuentas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" onclick="window.location='{{ route('Monitoreo') }}'; return false;">Monitoreo</a>
                </li>
            </ul>
            </form>
        </div>
    </nav>

    <div class="container mt-4">

    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
        @yield('content')
</body>
</html>
