@extends('dashboard_inicio')

@section('content')
    <h1>Realizar Depósito</h1>
    <div class="container mt-4">
        <form id="formulario">
            @csrf
            <div class="mb-3">
                <input type="number" name="cuentaOrigen" placeholder="Ingrese el número de cuenta de origen" class="form-control" required>
            </div>
            <div class="mb-3">
                <input type="number" name="cuentaDestino" placeholder="Ingrese el número de cuenta de destino" class="form-control" required>
            </div>
            <div class="mb-3">
                <input type="number" name="valorDeposito" placeholder="Ingrese el valor a depositar" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary">Realizar Depósito</button>
        </form>
        <h1 id="mensaje" style="text-align: center;"></h1>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#formulario").submit(function(event) {
                event.preventDefault();
                var formData = {
                    cuentaOrigen: $("input[name='cuentaOrigen']").val(),
                    cuentaDestino: $("input[name='cuentaDestino']").val(),
                    valorDeposito: $("input[name='valorDeposito']").val(),
                    _token: $("input[name='_token']").val()
                };

                $.ajax({
                    url: "{{ route('realizarDeposito') }}",
                    type: "POST",
                    data: formData,
                    success: function(response) {
                        $("#mensaje").text("Se reaalizo el deposito con exito.");
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#mensaje").text("Error: "  + errorThrown);
                        console.error("Error: " + errorThrown);
                    }
                });
            });
        });
    </script>
@endsection
