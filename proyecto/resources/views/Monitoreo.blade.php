@extends('dashboard_inicio')
@section('content')
    <h1>Monitoreo</h1>
    <div class="container mt-4">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Nombre Cliente</th>
                    <th scope="col">Número de Cuenta</th>
                    <th scope="col">Fecha Transacción</th>
                    <th scope="col">Tipo Transacción</th>
                    <th scope="col">Monto</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($resultado as $transaccion)
                <tr>
                    <td>{{ $transaccion->nombres }} {{ $transaccion->apellidos }}</td>
                    <td>{{ $transaccion->numero_cuenta }}</td>
                    <td>{{ $transaccion->fecha }}</td>
                    <td>{{ $transaccion->tipo_transaccion }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
