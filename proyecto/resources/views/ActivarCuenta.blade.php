@extends('dashboard_inicio')
@section('content')
    <h1>Activar Cuenta</h1>
    <div class="container mt-4">
        <form method="POST" id="formulario" action="{{ route('enviarActivarCuenta') }}">
            @csrf
            <div class="mb-3">
                <input type="text" name="numeroCuenta" placeholder="Ingrese el número de cuenta" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary">Activar Cuenta</button>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#formulario").submit(function(event) {
                event.preventDefault();
                var formData = {
                    numeroCuenta: $("input[name='numeroCuenta']").val(),
                    _token: $("input[name='_token']").val()
                };

                $.ajax({
                    url: "{{ route('enviarActivarCuenta') }}",
                    type: "POST",
                    data: formData,
                    success: function(response) {
                        alert("La cuenta se ha activado con éxito.");
                        window.location.href = "{{ route('inicio') }}";
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.error("Error: " + errorThrown);
                    }
                });
            });
        });
    </script>
@endsection
