@extends('dashboard_inicio')

@section('content')
    <h1>Todos los Clientes</h1>

    <h2>Cuentas Activas</h2>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Tipo de documento</th>
                <th scope="col">Numero de Identificacion</th>
                <th scope="col">Nombres</th>
                <th scope="col">Apellidos</th>
                <th scope="col">Razon Social</th>
                <th scope="col">Municipio</th>
                <th scope="col">Departamento</th>
                <th scope="col">Cuentas Activas</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($datos as $item)
                @if ($item->cuentas_activas > 0)
                    <tr>
                        <th scope="row">1</th>
                        <td>{{$item->tipo_documento}}</td>
                        <td>{{$item->numero_identificacion}}</td>
                        <td>{{$item->nombres}}</td>
                        <td>{{$item->apellidos}}</td>
                        <td>{{$item->razon_social}}</td>
                        <td>{{$item->municipio}}</td>
                        <td>{{$item->departamento}}</td>
                        <td>{{$item->cuentas_activas}}</td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

    <h2>Cuentas Inactivas</h2>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Tipo de documento</th>
                <th scope="col">Numero de Identificacion</th>
                <th scope="col">Nombres</th>
                <th scope="col">Apellidos</th>
                <th scope="col">Razon Social</th>
                <th scope="col">Municipio</th>
                <th scope="col">Departamento</th>
                <th scope="col">Cuentas Inactivas</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($datos as $item)
                @if ($item->cuentas_inactivas > 0)
                    <tr>
                        <th scope="row">1</th>
                        <td>{{$item->tipo_documento}}</td>
                        <td>{{$item->numero_identificacion}}</td>
                        <td>{{$item->nombres}}</td>
                        <td>{{$item->apellidos}}</td>
                        <td>{{$item->razon_social}}</td>
                        <td>{{$item->municipio}}</td>
                        <td>{{$item->departamento}}</td>
                        <td>{{$item->cuentas_inactivas}}</td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
@endsection
