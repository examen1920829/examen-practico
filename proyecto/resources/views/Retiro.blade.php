@extends('dashboard_inicio')

@section('content')
    <h1>Realizar Retiro</h1>
    <div class="container mt-4">
        <form id="formulario">
            @csrf
            <div class="mb-3">
                <input type="number" name="numeroCuenta" placeholder="Ingrese el número de cuenta" class="form-control" required>
            </div>
            <div class="mb-3">
                <input type="number" name="valorRetiro" placeholder="Ingrese el valor a retirar" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary">Realizar Retiro</button>
        </form>
        <h1 id="mensaje" style="text-align: center;"></h1>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#formulario").submit(function(event) {
                event.preventDefault();
                var formData = {
                    numeroCuenta: $("input[name='numeroCuenta']").val(),
                    valorRetiro: $("input[name='valorRetiro']").val(),
                    _token: $("input[name='_token']").val()
                };

                $.ajax({
                    url: "{{ route('realizarRetiro') }}",
                    type: "POST",
                    data: formData,
                    success: function(response) {
                        $("#mensaje").text("Se realizó el retiro exitosamente.");
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#mensaje").text("Error: "  + errorThrown);
                        console.error("Error: " + errorThrown);
                        
                    }
                });
            });
        });
    </script>
@endsection
