@extends('dashboard_inicio')

@section('content')
    <h1>Consultar Saldo</h1>
    <div class="container mt-4">
        <form id="formulario">
            @csrf
            <div class="mb-3">
                <input type="number" name="numeroCuenta" placeholder="Ingrese el número de cuenta" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary">Consultar Saldo</button>
        </form>
    </div>
    <div class="container mt-4">
        <h1 id="saldo" style="text-align: center;"></h1>
        <table class="table table-striped-columns">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Tipo de Transacción</th>
                    <th scope="col">Fecha</th>
                </tr>
            </thead>
            <tbody id="tablaTransacciones">
            </tbody>
        </table>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#formulario").submit(function(event) {
                event.preventDefault();
                var formData = {
                    numeroCuenta: $("input[name='numeroCuenta']").val(),
                    _token: $("input[name='_token']").val()
                };

                $.ajax({
                    url: "{{ route('consultarSaldo') }}",
                    type: "POST",
                    data: formData,
                    success: function(response) {
                        $("#saldo").text("Saldo: " + response.saldo);

                        var tablaTransacciones = $("#tablaTransacciones");
                        tablaTransacciones.empty(); 

                        $.each(response.transacciones, function(index, transaccion) {
                            var row = "<tr>" +
                                "<th scope='row'>" + (index + 1) + "</th>" +
                                "<td>" + transaccion.tipo_transaccion + "</td>" +
                                "<td>" + transaccion.fecha + "</td>" +
                                "</tr>";

                            tablaTransacciones.append(row);
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.error("Error: " + errorThrown);
                    }
                });
            });
        });
    </script>
@endsection
