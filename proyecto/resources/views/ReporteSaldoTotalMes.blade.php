@extends('dashboard_inicio')
@section('content')
    <h1>Total de Saldo por Mes</h1>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Mes</th>
                <th scope="col">Saldo Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($datos as $item)
                <tr>
                    <td>{{ $item->mes }}</td>
                    <td>{{ $item->saldo_total }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
