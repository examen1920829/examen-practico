@extends('dashboard_inicio')
@section('content')
    <h1>Creación de Cuenta de Ahorros</h1>
    <div class="container mt-4">
        <form method="POST" id="formulario" action="{{ route('enviarCrearCuenta') }}">
            @csrf
            <div class="mb-3">
                <input type="number" name="numeroIdentificacion" placeholder="Ingrese el número de identificación" class="form-control" required>
            </div>
            <div class="mb-3">
                <input type="number" name="monto" placeholder="Ingrese el monto inicial" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary">Crear Cuenta</button>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#formulario").submit(function(event) {
                event.preventDefault();
                var formData = {
                    numeroIdentificacion: $("input[name='numeroIdentificacion']").val(),
                    monto: $("input[name='monto']").val(),
                    _token: $("input[name='_token']").val()
                };

                $.ajax({
                    url: "{{ route('enviarCrearCuenta') }}",
                    type: "POST",
                    data: formData,
                    success: function(response) {
                        alert("Se creo la cuenta con exito.");
                        window.location.href = "{{ route('inicio') }}";
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.error("Error:" + errorThrown);
                    }
                });
            });
        });
    </script>
@endsection
