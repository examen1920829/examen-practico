@extends('dashboard_inicio')
@section('content')
    <h1>
        Seleccione el Tipo de Transaccion
    </h1>
    <form method="GET">
    <div class="container mt-4 d-flex justify-content-center">
        <button class="btn btn-primary btn-lg mx-3" onclick="window.location='{{ route('Retiro') }}'; return false;">Retiro</button>
    </form>
        <button class="btn btn-primary btn-lg mx-3" onclick="window.location='{{ route('Deposito') }}'; return false;">Deposito</button>
    </div>
    
@endsection
