@extends('dashboard_inicio')
@section('content')
    <h1>Clientes con Saldo Mayor a 20,000 y Depósito en los Últimos 15 Días</h1>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Tipo de documento</th>
                <th scope="col">Número de Identificación</th>
                <th scope="col">Nombres</th>
                <th scope="col">Apellidos</th>
                <th scope="col">Razón Social</th>
                <th scope="col">Municipio</th>
                <th scope="col">Departamento</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($datos as $item)
                <tr>
                    <th scope="row">1</th>
                    <td>{{ $item->tipo_documento }}</td>
                    <td>{{ $item->numero_identificacion }}</td>
                    <td>{{ $item->nombres }}</td>
                    <td>{{ $item->apellidos }}</td>
                    <td>{{ $item->razon_social }}</td>
                    <td>{{ $item->municipio }}</td>
                    <td>{{ $item->departamento }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
