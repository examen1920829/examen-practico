CREATE TABLE Usuarios (
	numero_identificacion VARCHAR(20) PRIMARY KEY,
    id SERIAL,
    tipo_documento VARCHAR(50),
    nombres VARCHAR(100),
    apellidos VARCHAR(100),
    razon_social VARCHAR(150),
    municipio VARCHAR(100),
    departamento VARCHAR(100),
    contrasena VARCHAR(255)
);


CREATE TABLE Cuentas (
    numero_cuenta VARCHAR(50)  PRIMARY KEY,
	id SERIAL,
    tipo_cuenta VARCHAR(50),
    saldo NUMERIC(10, 2),
    activo BOOLEAN,
    usuario_numero_identificacion VARCHAR(20) REFERENCES Usuarios(numero_identificacion) ON DELETE CASCADE
);


CREATE TABLE Transacciones (
    id SERIAL PRIMARY KEY,
    tipo_transaccion VARCHAR(50),
    fecha DATE,
    cuenta_numero_cuenta VARCHAR(50) REFERENCES Cuentas(numero_cuenta) ON DELETE CASCADE
);


